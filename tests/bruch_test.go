package tests

import (
	"github.com/linalinn/Bruch/bruch"
	"reflect"
	"testing"
)

func TestBruch_String(t *testing.T) {
	b1, _ := bruch.NewBruch(90, 20)
	if b1.String() != "4 1/2" {
		t.Fail()
	}

}

func TestBruch_Add(t *testing.T) {
	b1, _ := bruch.NewBruch(285, 45)
	b2, _ := bruch.NewBruch(170, 50)
	expected, _ := bruch.NewBruch(146, 15)

	result := b1.Add(b2)

	if !reflect.DeepEqual(expected, result) {
		t.Errorf("Expected: %s Got: %s", expected, result)
	}

}

func TestBruch_Sub(t *testing.T) {
	b1, _ := bruch.NewBruch(30, 39)
	b2, _ := bruch.NewBruch(16, 9)
	expected, _ := bruch.NewBruch(118, -117)

	result := b1.Sub(b2)

	if !reflect.DeepEqual(expected, result) {
		t.Errorf("Expected: %s Got: %s", expected, result)
	}

}

func TestBruch_Mul(t *testing.T) {
	b1, _ := bruch.NewBruch(72, 99)
	b2, _ := bruch.NewBruch(25, 65)
	expected, _ := bruch.NewBruch(40, 143)

	result := b1.Mul(b2)

	if !reflect.DeepEqual(expected, result) {
		t.Errorf("Expected: %s Got: %s", expected, result)
	}

}

func TestBruch_Div(t *testing.T) {
	b1, _ := bruch.NewBruch(84, 35)
	b2, _ := bruch.NewBruch(12, 90)
	expected, _ := bruch.NewBruch(18, 1)

	result, err := b1.Div(b2)
	if err != nil {
		t.Fail()
	}

	if !reflect.DeepEqual(expected, result) {
		t.Errorf("Expected: %s Got: %s", expected, result)
	}

}
